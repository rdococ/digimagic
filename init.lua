local rtc_nodebox =
{
	type = "fixed",
	fixed = {
		{ -8/16, -8/16, -8/16, 8/16, -7/16, 8/16 }, -- bottom slab

		{ -7/16, -7/16, -7/16, 7/16, -5/16,  7/16 },
	}
}

local rtc_selbox =
{
	type = "fixed",
	fixed = {{ -8/16, -8/16, -8/16, 8/16, -3/16, 8/16 }}
}

local mic_digiline_receive = function (pos, node, channel, msg)
	local setchan = minetest.get_meta(pos):get_string("channel")
	if type(msg) ~= "table" or (not msg.position) or type(msg.position) ~= "table" then return end
	local protected = minetest.is_protected(msg.position, minetest.get_meta(pos):get_string("owner"))
	if channel == setchan then
		if msg.command == "set_node" and msg.position and msg.node and (not protected) then
			minetest.set_node(msg.position, msg.node)
		elseif msg.command == "get_node" and msg.position then
			digiline:receptor_send(pos, digiline.rules.default, channel, minetest.get_node(msg.position))
		elseif msg.command == "set_meta" and msg.position and msg.key and msg.value and (not protected) then
			if msg.type == "string" then
				minetest.get_meta(msg.position):set_string(msg.key, msg.value)
			elseif msg.type == "int" or msg.type == "integer" then
				minetest.get_meta(msg.position):set_int(msg.key, msg.value)
			elseif msg.type == "float" or msg.type == "number" then
				minetest.get_meta(msg.position):set_float(msg.key, msg.value)
			end
		elseif msg.command == "get_meta" and msg.position and msg.key then
			if msg.type == "string" then
				digiline:receptor_send(pos, digiline.rules.default, channel, minetest.get_meta(msg.position):get_string(msg.key))
			elseif msg.type == "int" or msg.type == "integer" then
				digiline:receptor_send(pos, digiline.rules.default, channel, minetest.get_meta(msg.position):get_int(msg.key))
			elseif msg.type == "float" or msg.type == "number" then
				digiline:receptor_send(pos, digiline.rules.default, channel, minetest.get_meta(msg.position):get_float(msg.key))
			end
		end
		--digiline:receptor_send(pos, digiline.rules.default, channel, minetest.env)
	end
end
local gps_digiline_receive = function (pos, node, channel, msg)
	local setchan = minetest.get_meta(pos):get_string("channel")
	if channel == setchan and msg == "get_pos" then
		digiline:receptor_send(pos, digiline.rules.default, channel, pos)
	end
end
minetest.register_node("digimagic:mic", {
	description = "Minetest Interface Card",
	drawtype = "nodebox",
	tiles = {"digimagic_mic.png"},

	paramtype = "light",
	paramtype2 = "facedir",
	groups = {dig_immediate=2},
	selection_box = rtc_selbox,
	node_box = rtc_nodebox,
	digiline = 
	{
		receptor = {},
		effector = {
			action = mic_digiline_receive
		},
	},
	on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		meta:set_string("formspec", "field[channel;Channel;${channel}]")
	end,
	after_place_node = function(pos, placer, itemstack, pointed_thing)
		local meta = minetest.get_meta(pos)
		meta:set_string("owner", placer:get_player_name())
	end,
	on_receive_fields = function(pos, formname, fields, sender)
		local name = sender:get_player_name()
		if minetest.is_protected(pos, name) and not minetest.check_player_privs(name, {protection_bypass=true}) then
			minetest.record_protection_violation(pos, name)
			return
		end
		if (fields.channel) then
			minetest.get_meta(pos):set_string("channel", fields.channel)
		end
	end,
})
minetest.register_node("digimagic:gps", {
	description = "Global Positioning System",
	drawtype = "nodebox",

	paramtype = "light",
	paramtype2 = "facedir",
	groups = {dig_immediate=2},
	tiles = {
		"digimagic_gps_top.png",
		"jeija_microcontroller_bottom.png",
		"jeija_microcontroller_sides.png",
		"jeija_microcontroller_sides.png",
		"jeija_microcontroller_sides.png",
		"jeija_microcontroller_sides.png"
	},
	drawtype = "nodebox",
	selection_box = {
		--From luacontroller
		type = "fixed",
		fixed = { -8/16, -8/16, -8/16, 8/16, -5/16, 8/16 },
	},
	node_box = {
		--From Luacontroller
		type = "fixed",
		fixed = {
			{-8/16, -8/16, -8/16, 8/16, -7/16, 8/16}, -- Bottom slab
			{-5/16, -7/16, -5/16, 5/16, -6/16, 5/16}, -- Circuit board
			{-3/16, -6/16, -3/16, 3/16, -5/16, 3/16}, -- IC
		}
	},
	digiline = 
	{
		receptor = {},
		effector = {
			action = gps_digiline_receive
		},
	},
	on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		meta:set_string("formspec", "field[channel;Channel;${channel}]")
	end,
	on_receive_fields = function(pos, formname, fields, sender)
		local name = sender:get_player_name()
		if minetest.is_protected(pos, name) and not minetest.check_player_privs(name, {protection_bypass=true}) then
			minetest.record_protection_violation(pos, name)
			return
		end
		if (fields.channel) then
			minetest.get_meta(pos):set_string("channel", fields.channel)
		end
	end,
})

