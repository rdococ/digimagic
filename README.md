# digimagic
Digimagic mod for Minetest with new, magical nodes.

## MIC

The MIC stands for the Minetest Interface Card, and it allows a Luacontroller to modify the world around it. You can send messages such as "set_node", "get_node", "set_meta" and "get_meta". The basic format for each message are these:

{command="set_node", position={x=1, y=2, z=3}, node={name="default:node"}}

{command="get_node", position={x=1, y=2, z=3}}

{command="set_meta", position={x=1, y=2, z=3}, key="a", type="string", value="yay"}
(Type can either be "string", "int"/"integer", or "float"/"number".)

{command="get_meta", position={x=1, y=2, z=3}, key="energy", type="float"}
(Ditto.)

Note that the MIC has safeguards to prevent it from modifying protected areas that its owner does not own. There is no need to worry about such a thing ever happening, unless you use a badly coded areas mod that doesn't modify minetest.is_protected.

## GPS

The GPS is the Global Positioning System, like a real-life GPS. It broadcasts its own (not the luacontroller's) position when "get_pos" (not {command="get_pos"}, as in the MIC) is sent to it.
